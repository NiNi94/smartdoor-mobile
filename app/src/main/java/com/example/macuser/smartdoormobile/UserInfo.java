package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserInfo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserInfo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserInfo extends android.support.v4.app.Fragment {
    final String PREFERENCES_ID = "user-preferences";
    SharedPreferences preferences;

    private View view;
    private Context context;
    private Bundle saved;
    private Activity activity;

    private UserInfoRequest userEditInfo;

    private TextView userName;
    private TextView surName;
    private TextView adress;
    private Button buttonEdit;

    public UserInfo() {

    }

    public void setRequestUserInfo(final UserInfoRequest userEditInfo) {
        this.userEditInfo = userEditInfo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = container.getContext();
        this.saved = savedInstanceState;
        this.activity = this.getActivity();
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_user_info, container, false);

        this.userName = (TextView) this.view.findViewById(R.id.name_textview_info_label_pref);
        this.surName = (TextView) this.view.findViewById(R.id.surname_textview_info_label_pref);
        this.adress = (TextView) this.view.findViewById(R.id.adress_textview_info_label_pref);
        this.buttonEdit = (Button) this.view.findViewById(R.id.edit_button_info);

        this.buttonEdit.setOnClickListener(e -> {
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.user_data_frame, this.userEditInfo);
            ft.commit();
        });

        this.preferences = this.activity.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        this.userName.setText(preferences.getString("USER_NAME", "Unavaiable"));
        this.surName.setText(preferences.getString("USER_SURNAME", "Unavaiable"));
        this.adress.setText(preferences.getString("USER_ADRESS", "Unavaiable"));
        return this.view;
    }



}
