package com.example.macuser.smartdoormobile.bluetooth;

/**
 * Created by macuser on 01/02/18.
 */

public interface BtObserver {
    public void udpateTemp(String temp);
    public void serverLoginCheck(boolean login);
    public void welcomeUser();
}
