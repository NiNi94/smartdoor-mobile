package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserInfoRequest.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserInfoRequest#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserInfoRequest extends android.support.v4.app.Fragment{
    final String PREFERENCES_ID = "user-preferences";

    private View view;
    private Context context;
    private Bundle saved;
    private Activity activity;

    private UserInfo info;

    private Button buttonSave;
    private EditText userName;
    private EditText userSurname;
    private EditText userAdress;

    public UserInfoRequest(final UserInfo info) {
        this.info = info;
        this.info.setRequestUserInfo(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = container.getContext();
        this.saved = savedInstanceState;
        this.activity = this.getActivity();

        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_user_info_request, container, false);

        this.userAdress = (EditText)this.view.findViewById(R.id.user_adress_text);
        this.userName = (EditText)this.view.findViewById(R.id.user_name_text);
        this.userSurname = (EditText)this.view.findViewById(R.id.user_surname_text);
        this.buttonSave = (Button)this.view.findViewById(R.id.save_button_info);
        this.buttonSave.setOnClickListener(e -> {
            this.setUserSetting();
            Log.v("log", UserInfoRequest.this.activity.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE).getAll().toString());
        });
        return this.view;
    }

    private void setUserSetting() {
        if (!this.userSurname.getText().toString().equals("") && !this.userName.getText().toString().equals("") && !this.userAdress.getText().toString().equals("")) {
            Log.v("lgo", "entrato");
            SharedPreferences preferences = this.activity.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
            preferences.edit().putString("USER_NAME", this.userName.getText().toString())
                              .putString("USER_SURNAME", this.userSurname.getText().toString())
                              .putString("USER_ADRESS", this.userAdress.getText().toString())
                              .commit();
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.user_data_frame, this.info);
            ft.commit();
        }
    }


}
