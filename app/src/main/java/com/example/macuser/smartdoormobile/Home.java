package com.example.macuser.smartdoormobile;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Home.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home extends android.support.v4.app.Fragment {

    final String PREFERENCES_ID = "user-preferences";
    private View view;
    private Context context;
    private Bundle saved;
    private Activity activity;
    private ImageView imageView;
    private Button buttonBack;
    DrawerLayout drawer;

    private UserInfoRequest infoRequest;
    private UserInfo info;




    public Home() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = container.getContext();
        this.saved = savedInstanceState;
        this.activity = this.getActivity();

        this.info = new UserInfo();
        this.infoRequest = new UserInfoRequest(this.info);

        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_home, container, false);
        this.buttonBack = (Button)this.view.findViewById(R.id.button_back_home);
        this.drawer = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        this.buttonBack.setOnClickListener(t -> {
            if (this.drawer.isDrawerOpen(GravityCompat.START)) {
                this.drawer.closeDrawer(GravityCompat.START);
            } else {
                this.drawer.openDrawer(GravityCompat.START);
            }
        });

        if (!this.activity.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE).getAll().isEmpty()) {
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.user_data_frame, this.info);
            ft.commit();
        } else {
            android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.user_data_frame, this.infoRequest);
            ft.commit();
        }
        return this.view;
    }





}
