package com.example.macuser.smartdoormobile.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.macuser.smartdoormobile.R;

import java.util.List;

/**
 * Created by macuser on 29/01/18.
 */

public class BluetoothAdapterz extends ArrayAdapter<BluetoothDevice> {
    public BluetoothAdapterz(Context context, List<BluetoothDevice> devices) {
        super(context, 0, devices);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        BluetoothDevice device = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.device_name);
        TextView tvHome = (TextView) convertView.findViewById(R.id.device_adress);
        // Populate the data into the template view using the data object
        tvName.setText(device.getName());
        tvHome.setText(device.getAddress());
        // Return the completed view to render on screen
        return convertView;
    }
}